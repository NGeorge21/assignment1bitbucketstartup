# Nissi George Vairamon Student ID: 8758766

# Todo App

## Usage Instructions

- Put Task in Input bar.
- Click on the plus sign.
- When done with the task click the check mark.
- To delete the task click on the garbage can.

## How to Open

- Open index1.html in browser.

## Added MIT License to the Project

- It is the most open license available. Permissions are available for Commercial use, distribution,modification and private use.
