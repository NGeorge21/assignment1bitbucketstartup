let count = 0;

function handleAdd() {
  const index = count++;    /* TO BE ABLE TO UPDATE THE NUMBERING IF THE TASK IS NOT ENTERED */ 
  const title = $("#input").val() || index;
  $("#todos").append(
    `
    <div id=todo-${index} class="todo">
      <button onclick="handleComplete(${index})" class="fas fa-check"></button>
      <button onclick="handleRemove(${index})" class="fas fa-trash"></button>
      ${title}
    </div>
    `
  );

  $('#input').val('');
}
/* HANDLING THE REMOVE TASK FUNCTION*/ 
function handleRemove(index) {
  console.log(`handleRemove(${index})`);
  $(`#todo-${index}`).fadeOut(200);
  count--;
}
/* HANDLING THE COMPLETE TASK FUNCTION*/ 
function handleComplete(index) {
  console.log(`handleComplete(${index})`);
  $(`#todo-${index}`).toggleClass('checked');
}
/* HANDLING THE FUNCTION OF THE ADD TASK BUTTON*/ 
$(function(){
  $("#add").click(handleAdd);
}); 


